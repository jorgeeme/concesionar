/**
 * View Models used by Spring MVC REST controllers.
 */
package com.concesionaria.myapp.web.rest.vm;
