package com.concesionaria.myapp.service;

import com.concesionaria.myapp.domain.Empleado;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link Empleado}.
 */
public interface EmpleadoService {
    /**
     * Save a empleado.
     *
     * @param empleado the entity to save.
     * @return the persisted entity.
     */
    Empleado save(Empleado empleado);

    /**
     * Partially updates a empleado.
     *
     * @param empleado the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Empleado> partialUpdate(Empleado empleado);

    /**
     * Get all the empleados.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Empleado> findAll(Pageable pageable);

    /**
     * Get all the empleados by activo = true.
     *
     * @param activo the pagination information.
     * @return the list of entities.
     */
    List<Empleado> findAllByActivoTrue();

    /**
     * Get all the empleados by activo = false.
     *
     * @param activo the pagination information.
     * @return the list of entities.
     */
    List<Empleado> findAllByActivoFalse();

    /**
     * Get the "id" empleado.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Empleado> findOne(Long id);

    /**
     * Delete the "id" empleado.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
