package com.concesionaria.myapp.repository;

import com.concesionaria.myapp.domain.Empleado;
import java.util.List;
import org.springdoc.core.converters.models.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Empleado entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Long> {
    List<Empleado> findAllByActivoTrue();
    List<Empleado> findAllByActivoFalse();
}
